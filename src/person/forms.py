# coding: utf-8
from django import forms
from .models import Person


class PersonForm(forms.ModelForm):

    class Meta:
        model = Person
        fields = (
            'last_name',
            'first_name',
            'middle_name',
            'date_of_birth',
            'date_of_death',
            'email',
        )
