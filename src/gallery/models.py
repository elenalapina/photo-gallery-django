from django.db import models


class Photo(models.Model):
    url = models.URLField(
        'URL изображения',
    )
    comment = models.TextField(
        'Описание фотографии',
    )

    class Meta:
        verbose_name = 'Фотография'
        verbose_name_plural = 'Фотографии'
