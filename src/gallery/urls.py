from django.conf.urls import url, include

from .views import edit_form
from .views import index
from .views import save_photo


urlpatterns = [
    url(r'^$', index),
    url(r'add$', edit_form, name='add-form'),
    url(r'edit/(\d+)$', edit_form, name='edit-form'),
    url(r'save$', save_photo, name='add-photo'),
    url(r'save/(\d+)$', save_photo, name='edit-photo'),
]
